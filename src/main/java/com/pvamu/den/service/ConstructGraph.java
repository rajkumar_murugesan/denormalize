package com.pvamu.den.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pvamu.den.dal.UserRelationDALImpl;
import com.pvamu.den.model.AuxliaryUser;
import com.pvamu.den.model.CompareNode;
import com.pvamu.den.model.Data;
import com.pvamu.den.model.Data_;
import com.pvamu.den.model.Edge;
import com.pvamu.den.model.Elements;
import com.pvamu.den.model.Node;
import com.pvamu.den.model.TargetedUsers;
import com.pvamu.den.model.UserRelation;

@Service
public class ConstructGraph {

	@Autowired
	private UserRelationDALImpl userRelationDALImpl;

	public Elements graphCreate(Map<String, CompareNode> mapCompare, Map<Long, TargetedUsers> mapTargetUsers,Map<Long, AuxliaryUser> mapAuxUsers) {

		List<Node> nodes = new ArrayList<>();
		List<Edge> edges = new ArrayList<>();

		List<Long> tUser = new ArrayList<>();
		List<Long> aUser = new ArrayList<>();

		List<String> taEdge = new ArrayList<>();

		Elements ele = new Elements();
		
		
		final String tlineColor = "#8EE6EC";
		final String tlineStyle = "solid";

		
		for (Long key : mapTargetUsers.keySet()) {
			
			addNode((long)mapTargetUsers.get(key).getAlterId(), nodes, "#46C0D8");
			
			List<UserRelation> target_relations = userRelationDALImpl.getUsersByParentId(key);

			for (UserRelation targetUser : target_relations) {
				if (mapTargetUsers.containsKey(targetUser.getChild())) {
					TargetedUsers tuchildEdge = mapTargetUsers.get(targetUser.getChild());

					edges.add(constrctEdge(tuchildEdge.getAlterId(), mapTargetUsers.get(key).getAlterId(), tlineColor, tlineStyle, "Target Relation", taEdge));
				}

			}
			
			}
		
		for (Long key : mapAuxUsers.keySet()) {
			
			addNode((long)mapAuxUsers.get(key).getUserId(), nodes, "#CD8E58");
			
			List<UserRelation> aux_relations = userRelationDALImpl.getUsersByParentId(key);
			for (UserRelation targetUser : aux_relations) {
				if (mapAuxUsers.containsKey(targetUser.getChild())) {
					AuxliaryUser tuchildEdge = mapAuxUsers.get(targetUser.getChild());
					edges.add(constrctEdge(tuchildEdge.getUserId(), key, "#C7891F", tlineStyle, "Auxiliary Relation", taEdge));
				}

			}
			
			}
		
		final String alineColor = "#888B88"; //F5A25A
		final String alineStyle = "dashed";

		for (String key : mapCompare.keySet()) {
		


//			if (!tUser.contains(mapCompare.get(key).getpTargetNode())) {
//				tUser.add(mapCompare.get(key).getpTargetNode());
//				addNode(mapCompare.get(key).getpTargetNode(), nodes, "#46C0D8");
//				List<UserRelation> target_relations = userRelationDALImpl.getUsersByParentId(mapCompare.get(key).getTargetNode());
//
//				for (UserRelation targetUser : target_relations) {
//					if (mapTargetUsers.containsKey(targetUser.getChild())) {
//						TargetedUsers tuchildEdge = mapTargetUsers.get(targetUser.getChild());
//
//						edges.add(constrctEdge(tuchildEdge.getAlterId(), mapCompare.get(key).getpTargetNode(), tlineColor, tlineStyle, "Target Relation", taEdge));
//					}
//
//				}
//
//			}

//			List<UserRelation> aux_relations = userRelationDALImpl.getUsersByParentId(mapCompare.get(key).getAuxNode());
//
//			if (!aUser.contains(mapCompare.get(key).getAuxNode())) {
//				aUser.add(mapCompare.get(key).getAuxNode());
//				addNode(mapCompare.get(key).getAuxNode(), nodes, "#CD8E58");
//
//				for (UserRelation targetUser : aux_relations) {
//					if (mapAuxUsers.containsKey(targetUser.getChild())) {
//						AuxliaryUser tuchildEdge = mapAuxUsers.get(targetUser.getChild());
//						edges.add(constrctEdge(tuchildEdge.getUserId(), mapCompare.get(key).getAuxNode(), "#C7891F", tlineStyle, "Auxiliary Relation", taEdge));
//					}
//
//				}
//
//			}
			String enzyme = conectEnzyme( mapTargetUsers.get(mapCompare.get(key).getTargetNode()), mapAuxUsers.get(mapCompare.get(key).getAuxNode()) );
			
			String lineColort= alineColor;
			if( mapCompare.get(key).isMatched()) {
				lineColort = "#FF0000";
			}
	
			edges.add(constrctEdge(mapCompare.get(key).getAuxNode(), mapCompare.get(key).getpTargetNode(), lineColort, alineStyle, enzyme, taEdge));
			

		}


		ele.setNodes(nodes);
		ele.setEdges(edges);

		return ele;

	}

	private Edge constrctEdge(long pUserId, long cUserId, String lineColor, String lineStyle, String enzyme,
			List<String> taEdge) {

		String ptu = Long.toString(pUserId);
		String tu = Long.toString(cUserId);
		String edgeId = ptu + tu;
		String rEdgeId = tu + ptu;
		if (taEdge.contains(edgeId)|| taEdge.contains(rEdgeId) || ptu.equals(tu)) {
			return null;
		}
		taEdge.add(edgeId);
		Edge edge = new Edge();
		Data_ data_ = new Data_();
		data_.setEnzyme(enzyme);
		data_.setId(edgeId);
		data_.setSource(ptu);
		data_.setTarget(tu);
		data_.setFaveColor(lineColor);
		data_.setLine_style(lineStyle);// solid, dotted, or dashed.
		// data_.setWeight(index1);
		edge.setData(data_);

		return edge;

	}

	private String conectEnzyme(TargetedUsers targetUser, AuxliaryUser auxUser) {

		StringBuilder value = new StringBuilder();
//		value.append("<table>");
		//	value.append("<tr><td>");
		value.append("ID #");
	//	value.append("</td><td>");
		
		value.append(targetUser.getUserId());
		value.append(" , "); 
		value.append(auxUser.getUserId());
		value.append("\n");
	
		
		value.append("Dob#");
		value.append(targetUser.getDob());
		value.append(" , ");
		value.append(auxUser.getDob());
		value.append("\n");

		value.append("Gender#");
		value.append(targetUser.getGender());
		value.append(" , ");
		value.append(auxUser.getGender());
		value.append("\n");
		
		
		value.append("Degree #");
		value.append(targetUser.getxAxis());
		value.append(" , ");
		value.append(auxUser.getxAxis());
		value.append("\n");

		value.append("Centrality #");
		value.append(targetUser.getCentrality());
		value.append(" , ");
		value.append(auxUser.getCentrality());
		value.append("\n");
		//value.append("</table>");
		return value.toString();
	}

	private void addNode(Long userId, List<Node> nodes, String color) {
		Data node_data = new Data();
		node_data.setId(Long.toString(userId));
		node_data.setColor(color);
		Node nod = new Node();
		nod.setData(node_data);
		nodes.add(nod);

	}

}
