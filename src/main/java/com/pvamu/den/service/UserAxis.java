package com.pvamu.den.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pvamu.den.dal.UserDALImpl;
import com.pvamu.den.dal.UserRelationDALImpl;
import com.pvamu.den.model.User;
import com.pvamu.den.model.UserRelation;

@Service
public class UserAxis {
	
	@Autowired
	private  UserRelationDALImpl userRelationDALImpl;
	
	@Autowired
	private  UserDALImpl userDALImpl;

	int index = 0;
	
	
	public void findXAsis(User user , Map<Long, User> userMap , int expectedsize , List<Long> result , boolean tragettype) {
	// System.out.println("Index before "+ index );
		   List<UserRelation> childUsers = userRelationDALImpl.getUsersByParentId(user.getUserId());
	
		   if(expectedsize >= childUsers.size() && tragettype) {
			   return;
		   }
		   int xAxis = 1;
		   userMap.put(user.getUserId(), user);
		   
		   for (UserRelation userRelation : childUsers) {
			   //check and add relation ship reverse way 
			   addUnidirectionRelation(userRelation.getChild() ,user.getUserId() );
			   
			
			   
			   if(!userMap.containsKey(userRelation.getChild())){
				  User childUser =  userDALImpl.getUserById(userRelation.getChild());
				   if( childUser != null) {
					   if(result.contains(childUser.getUserId())) {
						   continue;
					   } 
					   
					   userMap.put(userRelation.getChild(), childUser);
					   xAxis = xAxis + 1;
				   }else {
					   userRelationDALImpl.removeChild(userRelation.getChild());
				   }
				   
				 }
		
		   }
		   user.setxAxis(xAxis);
		   findMore(userMap, expectedsize, result);
	}
	
	
	private void findMore(Map<Long, User> userMap, int expectedsize, List<Long> targetUsers) {

		if(userMap.size() >=  expectedsize ) {
			// System.out.println("Index after "+ index );
			return;
		}

		for (Long userId : userMap.keySet())  
		{ 
			// search  for value 
			User user = userMap.get(userId); 
			if( user.getxAxis() ==  0) {
				findXAsis(userDALImpl.getUserById(userId),userMap,expectedsize, targetUsers, false);  
				break;
			} 
		} 

	}
	
	
	private void addUnidirectionRelation(long parentId , long childId) {
		
		if(!userRelationDALImpl.isRelatedUniDirection(parentId, childId)) {
			UserRelation ur = new UserRelation();
			ur.setId(UUID.randomUUID().toString());
			ur.setChild(childId);
			ur.setParent(parentId);
			userRelationDALImpl.save(ur);
			index++;
		}
		
	}
	

}
