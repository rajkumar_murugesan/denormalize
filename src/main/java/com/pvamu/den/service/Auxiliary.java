package com.pvamu.den.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pvamu.den.dal.AuxliaryUserDALImpl;
import com.pvamu.den.dal.OverLpaUserDALImpl;
import com.pvamu.den.dal.TargerUserDALImpl;
import com.pvamu.den.dal.UserDALImpl;
import com.pvamu.den.dal.UserRelationDALImpl;
import com.pvamu.den.model.Anonymization;
import com.pvamu.den.model.AuxliaryUser;
import com.pvamu.den.model.OverLapuser;
import com.pvamu.den.model.TargetedUsers;
import com.pvamu.den.model.User;
import com.pvamu.den.model.UserRelation;


@Service
public class Auxiliary {
	
	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@Autowired
	private  TargerUserDALImpl targerUserDALImpl;
	
	@Autowired
	private  AuxliaryUserDALImpl auxliaryUserDALImpl;
	
	@Autowired
	private  OverLpaUserDALImpl overLpaUserDALImpl;
	
	@Autowired
	private  UserDALImpl userDALImpl;
	
	@Autowired
	private  UserRelationDALImpl userRelationDALImpl;

	@Autowired
	private EgenVectorC egenVectorC;
	
	@Autowired
	private UserAxis userXAxis ;
	
	public void generate(int auxiliarySize, int overlap) {
		
		auxliaryUserDALImpl.clearAllRecord();
		
		overLpaUserDALImpl.clearAllRecord();
		List<TargetedUsers> targetUsers = targerUserDALImpl.getTargetUser();
		LOG.info("User profile cout  : {}.", auxliaryUserDALImpl.collectionCount());
		
		
		float targetset = ( targetUsers.size() * overlap);
		
		float overlap_pre = targetset / 100;
		
		
		List<TargetedUsers> overlaptargetUser = new ArrayList<>();
		List<Long> targetUsersIds = new ArrayList<>();
		
		for( TargetedUsers user : targetUsers) {
			targetUsersIds.add(user.getUserId());
		}
		
		Map<Long , User> resultRecord = new LinkedHashMap<>();
		
		List<OverLapuser> overlapUser = new ArrayList<>();
		
		List<Long> overlapUserId = new ArrayList<>();
		
		overlaptargetUser = targetUsers.subList(0,(int) Math.floor(overlap_pre));
		
		overlaptargetUser.forEach(user -> {
			
			resultRecord.put(user.getUserId(), user);
			OverLapuser tUsers = new OverLapuser();
			tUsers.setGender(user.getGender());
			tUsers.setDob(user.getDob());
			tUsers.setUserId(user.getUserId());
			overlapUser.add(tUsers);
			overlapUserId.add(user.getUserId());
		});
		
		for (int i = 0 ; i < overlaptargetUser.size() ; i++) {
			userXAxis.findXAsis(userDALImpl.getUserById(overlaptargetUser.get(i).getUserId()), resultRecord, auxiliarySize , targetUsersIds , false);
			
			if(! (resultRecord.size() >= auxiliarySize)) {
				resultRecord.clear();
				overlaptargetUser.forEach(user -> {
				resultRecord.put(user.getUserId(), user);
				});
			}
			
		}

		List<AuxliaryUser> auxUser = new ArrayList<>();
		int index = 0;
		List<Long> listUserId = new LinkedList<>();
		for (Long userId : resultRecord.keySet())  
		{ 
			if(index >= auxiliarySize) {
				break;
			}
			listUserId.add(userId);
			index = index +1;
		}
		
		index = 0;
		List<Double>  targetCentrality = egenVectorC.getNormalisedEigenVectors(listUserId);
		for (Long userId : listUserId)  
	       { 
			
			User user = resultRecord.get(userId); 
	           
			AuxliaryUser tUsers = new AuxliaryUser();
			List<UserRelation> relations = userRelationDALImpl.getUsersByParentId(userId);
			
			int degree = 0;
			for(UserRelation  ur: relations) {
				if(listUserId.contains(ur.getChild()) ) {
					degree = degree+1;
				}
			}
			
			tUsers.setxAxis(degree);
			tUsers.setGender(user.getGender());
			tUsers.setDob(user.getDob());
			tUsers.setUserId(user.getUserId());
			tUsers.setCentrality(targetCentrality.get(index));
			auxUser.add(tUsers);
			index = index +1;
	       } 
		overLpaUserDALImpl.addOverloapUsers(overlapUser);		
		auxliaryUserDALImpl.addAuxliaryUsers(auxUser);	
		
	}

	
	public void getAlias(long id) {
		List<AuxliaryUser>  tUsers = auxliaryUserDALImpl.getAllRecord();
		
		
		List<Long> onlyId = new ArrayList(); 
			
	for(AuxliaryUser tlong : tUsers) {
		onlyId.add(tlong.getUserId());
	}
		
		targerUserDALImpl.getbyId(id);
		List<UserRelation> pR = userRelationDALImpl.getUsersByParentId(id);
		
		List<UserRelation> cR = userRelationDALImpl.getUsersByChildId(id);
		
		for(UserRelation tuserid : pR) {
			if(onlyId.contains(tuserid.getChild()) ) {
				System.out.println(tuserid.getChild());
			}
			
		}
		System.out.println("============");
		for(UserRelation tuserid : cR) {
			if(onlyId.contains(tuserid.getParent()) ) {
				System.out.println(tuserid.getParent());
			}
			
		}
	}
	

}
