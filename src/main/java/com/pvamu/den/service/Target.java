package com.pvamu.den.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pvamu.den.dal.TargerUserDALImpl;
import com.pvamu.den.dal.UserDALImpl;
import com.pvamu.den.dal.UserRelationDALImpl;
import com.pvamu.den.model.Anonymization;
import com.pvamu.den.model.TargetedUsers;
import com.pvamu.den.model.User;
import com.pvamu.den.model.UserRelation;

@Service
public class Target {
	private final Logger LOG = LoggerFactory.getLogger(getClass());

	@Autowired
	private  TargerUserDALImpl targerUserDALImpl;

	@Autowired
	private  UserRelationDALImpl userRelationDALImpl;

	@Autowired
	private  UserDALImpl userDALImpl;

	@Autowired
	private UserAxis userXAxis ;
	
	@Autowired
	private EgenVectorC egenVectorC;

	public void generate(int targetSize) {

		Map<Long , User> tartgetRecord = new LinkedHashMap<>();

		targerUserDALImpl.clearAllRecord();	

		LOG.info("========== User collectionCount : {}.", targerUserDALImpl.collectionCount());

		User  initRecoard= userDALImpl.getRandomRecoard();
		

		userXAxis.findXAsis(initRecoard, tartgetRecord,targetSize, new ArrayList<>() , true);

		if(tartgetRecord.size() < targetSize) {
			 this.generate(targetSize);
			 return;
		}
		int index = 0;
		List<Long> listUserId = new LinkedList<>();
		for (Long userId : tartgetRecord.keySet())  
		{ 
			if(index >= targetSize) {
				break;
			}
			index = index+1;
			listUserId.add(userId);
		}
		index = 0;
		List<Double>  targetCentrality = egenVectorC.getNormalisedEigenVectors(listUserId);
		
		for (Long userId : listUserId)  
		{ 
			
			User user = tartgetRecord.get(userId); 

			
			TargetedUsers tUsers = new TargetedUsers();
			
			List<UserRelation> relations = userRelationDALImpl.getUsersByParentId(userId);
			
			int degree = 0;
			for(UserRelation  ur: relations) {
				if(listUserId.contains(ur.getChild()) ) {
				degree = degree+1;
				}
			}
			
			
			tUsers.setxAxis(degree);
			tUsers.setGender(user.getGender());
			tUsers.setDob(user.getDob());
			tUsers.setUserId(user.getUserId());
			tUsers.setCentrality(targetCentrality.get(index));
			tUsers.setAlterId(index);
			targerUserDALImpl.addTargetUsers(tUsers);
			index = index+1;
		} 
	}

	
	


	public void anonymize(Anonymization  anonymization) {
	
		
		for ( TargetedUsers user : targerUserDALImpl.getTargetUser()){ 
			user.setGender(genderRangerCalc(user.getGender(),anonymization.getGenderRetain()));   
			user.setMaxYear(user.getDob() +  genderdobCalc(anonymization.getYearRange()));
			user.setMinYear(user.getDob() -  genderdobCalc(anonymization.getYearRange()));
			user.setDob(Math.round((user.getMaxYear()+ user.getMinYear()) / 2));
			targerUserDALImpl.updateTargetUsers(user);				
		}	

	}

	public List<TargetedUsers> getAllRecoard(){

		return targerUserDALImpl.getAllRecord();

	}

	private int genderRangerCalc(int gender , int passible) {
		Random rand = new Random();
		int gentor = 1;

		int randomNum = rand.nextInt(100 ) + 1;

		if (randomNum > passible) {
			
			if(gender==gentor) {
				gender=2; 
			}else {
				gender  = 1;
			}
		}
		return gender;

	}

	
	private int genderdobCalc(int passible) {
		
		passible = passible / 2 ;
		int randomNumber = (int)(Math.random() * (passible)) ;
	
		
		return randomNumber;

	}
	
	public void getAlias(long id) {
		List<TargetedUsers>  tUsers = targerUserDALImpl.getAllRecord();
		
		
		List<Long> onlyId = new ArrayList(); 
			
	for(TargetedUsers tlong : tUsers) {
		onlyId.add(tlong.getUserId());
	}
		
		targerUserDALImpl.getbyId(id);
		List<UserRelation> pR = userRelationDALImpl.getUsersByParentId(id);
		
		List<UserRelation> cR = userRelationDALImpl.getUsersByChildId(id);
		
		for(UserRelation tuserid : pR) {
			if(onlyId.contains(tuserid.getChild()) ) {
				System.out.println(tuserid.getChild());
			}
			
		}
		System.out.println("============");
		for(UserRelation tuserid : cR) {
			if(onlyId.contains(tuserid.getParent()) ) {
				System.out.println(tuserid.getParent());
			}
			
		}
	}


}
