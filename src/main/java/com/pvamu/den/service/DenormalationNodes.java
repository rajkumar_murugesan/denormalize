package com.pvamu.den.service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pvamu.den.dal.AuxliaryUserDALImpl;
import com.pvamu.den.dal.LogReportDALImpl;
import com.pvamu.den.dal.OverLpaUserDALImpl;
import com.pvamu.den.dal.TargerUserDALImpl;
import com.pvamu.den.dal.UserRelationDALImpl;
import com.pvamu.den.model.Analysis;
import com.pvamu.den.model.AuxliaryUser;
import com.pvamu.den.model.CompareNode;
import com.pvamu.den.model.DeAnonymizationResponse;
import com.pvamu.den.model.Elements;
import com.pvamu.den.model.LogReport;
import com.pvamu.den.model.OverLapuser;
import com.pvamu.den.model.TargetedUsers;
import com.pvamu.den.model.UserRelation;



@Service
public class DenormalationNodes {
	
	@Autowired
	private  TargerUserDALImpl targerUserDALImpl;
	
	@Autowired
	private  AuxliaryUserDALImpl auxliaryUserDALImpl;
	
	@Autowired
	private  OverLpaUserDALImpl overLpaUserDALImpl;
	
	@Autowired
	private  UserRelationDALImpl userRelationDALImpl;
	
	@Autowired
	private  LogReportDALImpl logReportDALImpl;
	
	
	@Autowired
	private  CosineSimilarity cosineSimilarity;
	
	@Autowired
	private  ConstructGraph constructGraph;
	
	
	Map<Long , TargetedUsers> mapTargetUsers = new LinkedHashMap<Long, TargetedUsers>();
	
	Map<Long , AuxliaryUser> mapAuxUsers = new LinkedHashMap<Long, AuxliaryUser>();
	
	Map<String, CompareNode> mapCompare = new LinkedHashMap<>();
	
	public DeAnonymizationResponse generate(int noSeed , int config , boolean keepPrev ) {
		clearData();
		//Prepare Data 
		List<Long> node_overLap =  overlapProcess(noSeed ,  keepPrev );
		targetProcess();
		auxProcess();

		      
		// initial seed for comapre 
		for(Long userid : node_overLap) {
				String id = ""+userid+""+userid; 
				CompareNode compareNode = new CompareNode(userid, userid ,mapTargetUsers.get(userid).getAlterId() , mapAuxUsers.get(userid).getUserId() );
				compareNode.setSimilartity(1);
				compareNode.setMatched(true);
				mapCompare.put(id , compareNode);
		}
		
		getchildNodes(config) ;
		//System.out.println(mapCompare.size());
		
		int accuracy = 0 ;
		for (String key : mapCompare.keySet()){
			
			if ( mapCompare.get(key).isMatched()) {
				accuracy = accuracy+1;
			}
		}
		List<OverLapuser> overlapUsers = overLpaUserDALImpl.getAllRecord();
		int avargeAcc = 0;
		if( (overlapUsers.size() - noSeed) > 0) { 
		
		 avargeAcc = ( (accuracy-noSeed) / (overlapUsers.size() - noSeed)  );
		}
		
		
		LogReport logrport = new LogReport();
		
		logrport.setNoSeeds(noSeed);
		logrport.setConfiguration(config);
		logrport.setPreSeed(keepPrev);
		logrport.setAccuracy(avargeAcc);
		logrport.setTotalMatched(mapCompare.size());
		
		logReportDALImpl.addLog(logrport);
		Elements elements = constructGraph.graphCreate(mapCompare , mapTargetUsers , mapAuxUsers);
				
		Analysis analysis = new Analysis();
		analysis.setNoAccuracy(accuracy);
		analysis.setNoMatched(mapCompare.size());
		analysis.setNoofA(mapAuxUsers.size());
		analysis.setNoofT(mapTargetUsers.size());
		
		DeAnonymizationResponse response = new DeAnonymizationResponse();
		response.setElements(elements);
		response.setAnalysis(analysis);
		clearData() ;
		
	return response;
		
	}
	
	
	
	private void getchildNodes(int config) {
		
		String lastKey = "";
		for (Map.Entry<String, CompareNode> entry : mapCompare.entrySet()) {
            lastKey = entry.getKey();
        }
		
		if( mapCompare.get(lastKey).isVisted()) {
			return;
		}
		
       
		Map<String, CompareNode> childmapCompare = new TreeMap<>();
		for (String key : mapCompare.keySet()){
			
			CompareNode node = mapCompare.get(key);
			if(node.isVisted()) {
				continue;
			}
			node.setVisted(true);
			List<UserRelation> target_relations = userRelationDALImpl.getUsersByParentId(node.getTargetNode());
			List<UserRelation> aux_relations = userRelationDALImpl.getUsersByParentId(node.getAuxNode());
			
			 for(UserRelation targetUser : target_relations) {				 
				 if( mapTargetUsers.containsKey(targetUser.getChild())) {
					 
					 
					 
					
					 for(UserRelation auxUser : aux_relations) {
						 if( mapAuxUsers.containsKey(auxUser.getChild())) {
							 	String reverseid = ""+auxUser.getChild()+""+targetUser.getChild(); 
							 	String id = ""+targetUser.getChild()+""+auxUser.getChild(); 
							 	
							 	boolean nodeIdentify = false ;
							 	
							 	for (String checkKey : mapCompare.keySet()){
									
									CompareNode checkNode = mapCompare.get(checkKey);
									
									if( (targetUser.getChild() == checkNode.getTargetNode() )|| ( auxUser.getChild() == checkNode.getAuxNode() )){
										nodeIdentify = true;
										break;
									}
									
								}
							 	if(childmapCompare.containsKey(id)||mapCompare.containsKey(reverseid) || nodeIdentify) {
							 		continue;
							 	}
							 	
							 	
							 	CompareNode compareNode = new CompareNode(targetUser.getChild() , auxUser.getChild() , mapTargetUsers.get(targetUser.getChild()).getAlterId() , node.getAuxNode() );
							 	compareNode.setSimilartity(cosineSimilarity.findcosineSimilarity(mapTargetUsers.get(targetUser.getChild()) ,mapAuxUsers.get(auxUser.getChild()) ,config ));
							 	if(compareNode.getAuxNode()== compareNode.getTargetNode()) {
							 		compareNode.setMatched(true);
								}
							 	childmapCompare.put(id , compareNode);
					
									
						 }
					 }
				 }
				 
			 }
			
				
			}
		
		
		Map<String, CompareNode> result = new TreeMap<>();
		highvalue(childmapCompare , result);
		
		mapCompare.putAll(result);
		
		getchildNodes(config);
		
	}
	
	
	
	private void highvalue(Map<String, CompareNode> childmapCompare , Map<String, CompareNode> result ) {
		
		if(childmapCompare.isEmpty()) {
			return;
		}
		
		
		Map<String, CompareNode> childmapCompare1 = new TreeMap<>(childmapCompare);
		
		double highVlue = 0;
		CompareNode last_node = null;
		for (String childKey : childmapCompare.keySet()){
		
			if( highVlue < childmapCompare.get(childKey).getSimilartity()) {
				highVlue = childmapCompare.get(childKey).getSimilartity();
				last_node =childmapCompare.get(childKey);
			}
		}
		if(last_node != null) {
			String id = ""+last_node.getTargetNode()+""+last_node.getAuxNode(); 
			
			result.put(id , last_node);
		}
		for (String childKey : childmapCompare.keySet()){
			
			if(last_node != null &&  childmapCompare.get(childKey).getTargetNode() == last_node.getTargetNode()) {
				childmapCompare1.remove(childKey);				
			}
			
			if(last_node != null &&  childmapCompare.get(childKey).getAuxNode() == last_node.getAuxNode()) {
							if(childmapCompare.get(childKey).getTargetNode() != last_node.getTargetNode()) {
					childmapCompare1.remove(childKey);
				}
			}			
		}
		highvalue(childmapCompare1 , result);
		
	}
	
	
	
	private void targetProcess() {
		List<TargetedUsers> targetUsers = targerUserDALImpl.getAllRecord();
		for(TargetedUsers seed:targetUsers) {			
			mapTargetUsers.put(seed.getUserId(), seed);

		}		
	}

	
	private List<Long> overlapProcess(int noSeed , boolean keepPrev ) {
		List<Long> selectedseed = new LinkedList<>();
		List<OverLapuser> overlapUsers = overLpaUserDALImpl.getAllRecord();
		
		if (keepPrev) {
			for(int x = 0 ; x < overlapUsers.size() ;x++) {
			if( overlapUsers.get(x).isSelectSeed()) {
				selectedseed.add(overlapUsers.get(x).getUserId());
				}
			}
		}
		else {
		List<Integer>  selectedIndex = new ArrayList<>();
		
		for(int x = 0 ; x < overlapUsers.size() ;x++) {
			
			overLpaUserDALImpl.updateTargetUsers(overlapUsers.get(x), false);
				
			}
		for(int x = 0 ; x < noSeed ;x++) {
			boolean flag = true ;
			while(flag) {
				int luckyIndex = (int) (Math.random()* overlapUsers.size());
			if(!selectedIndex.contains(luckyIndex)) {
				selectedIndex.add(luckyIndex);
				flag = false;
				selectedseed.add(overlapUsers.get(luckyIndex).getUserId());
				//overLpaUserDALImpl.addOverloapUsers(overlapUsers);
				overLpaUserDALImpl.updateTargetUsers(overlapUsers.get(luckyIndex), true);
				}
			}
		}
			
			//selectedseed.add(overlapUsers.get(x).getUserId());
			
		}
		return selectedseed;
	}
	
	
	private void auxProcess() {
		
		List<AuxliaryUser> auxUsers = auxliaryUserDALImpl.getAllRecord();
		for(AuxliaryUser seed:auxUsers) {							
			mapAuxUsers.put(seed.getUserId(), seed);

		}
		
		
	}
	
	private void clearData() {
		mapTargetUsers.clear();
		mapAuxUsers.clear();
		mapCompare.clear();
	}

	@Override
	protected void finalize() throws Throwable {
		clearData();
		super.finalize();
	}
	
	
	
	


}

