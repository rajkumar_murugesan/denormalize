package com.pvamu.den.model;

public class AuxUser {
	
  private int targetNode;	
	
  private int overlapPresentage ;
  
  private int auxNode;

public int getTargetNode() {
	return targetNode;
}

public void setTargetNode(int targetNode) {
	this.targetNode = targetNode;
}

public int getOverlapPresentage() {
	return overlapPresentage;
}

public void setOverlapPresentage(int overlapPresentage) {
	this.overlapPresentage = overlapPresentage;
}

public int getAuxNode() {
	return auxNode;
}

public void setAuxNode(int auxNode) {
	this.auxNode = auxNode;
}



}
