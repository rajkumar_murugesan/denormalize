package com.pvamu.den.dal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.pvamu.den.model.AuxliaryUser;
import com.pvamu.den.model.TargetedUsers;

@Repository
public class AuxliaryUserDALImpl {
	@Autowired
	private MongoTemplate mongoTemplate;

	
	public void addAuxliaryUsers(List<AuxliaryUser> user) {
		mongoTemplate.insertAll(user);
		
	}
	
	public void saveAuxliaryUsers(AuxliaryUser user) {
		
		mongoTemplate.save(user);	

	
}
	
	public long collectionCount() {
		Query query = new Query();
		query.getLimit();
		return mongoTemplate.count(query, AuxliaryUser.class);
	}
	
	public void clearAllRecord() {
		mongoTemplate.dropCollection(AuxliaryUser.class);
		
	}
	
	public List<AuxliaryUser> getAllRecord() {
		
		return mongoTemplate.findAll(AuxliaryUser.class);
	}
}
