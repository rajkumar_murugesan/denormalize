package com.pvamu.den.dal;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.pvamu.den.model.User;

@Repository
public class UserDALImpl {
	private final Logger LOG = LoggerFactory.getLogger(getClass());
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private UserRepository userRepo ;
	
	private List<Long> usedId = new ArrayList<Long>();

	public List<User> getAllUsersById(List<Long> userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(userId));
		return mongoTemplate.find(query, User.class);
	}
	
	
	public User getUserById(Long userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").is(userId));
		User user = mongoTemplate.findOne(query, User.class);
		return user;
	}
	
	public void addNewUser(List<User> user) {
		user.forEach(u -> {
			SaveUser(u);	
		});
		
	
	}
	
	public void SaveUser(User u) {
		mongoTemplate.save(u);	
	}

	public long collectionCount() {
		Query query = new Query();
		query.getLimit();
		return mongoTemplate.count(query, User.class);
	}
	
	public User getRandomRecoard() {
		try {
		Query query = new Query();
		long randomNumber = (long)(Math.random() * ((634761 - 100044) + 1)) ;
		if(usedId.contains(randomNumber)) {
			
			return getRandomRecoard();
		}
		usedId.add(randomNumber);
		query.addCriteria(Criteria.where("_id").is(randomNumber) );
		 
		List<User> userResponse = mongoTemplate.find(query, User.class);
		
		if(userResponse.isEmpty()) {
			return getRandomRecoard();
		}
		LOG.info("user profile {}.",userResponse.get(0).toString());
		
		return userResponse.get(0);
			
		}catch (Exception e) {
			getRandomRecoard();
		}
		return null;
	}


	public void resetUsedId() {
		this.usedId = new ArrayList<Long>();
	}
	
	public List<User> getAllUsersExcept(List<Long> userId , int auxrecordSize){
		
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").not().in(userId));
		query.limit(auxrecordSize);
		return mongoTemplate.find(query, User.class);
	}
	
	
	public User getFirstRecord() {
		
		Query query = new Query();
		query.addCriteria(Criteria.where("xAxis").exists(false));
		query.limit(1);			
		User user = mongoTemplate.findOne(query, User.class);
		return user;
		
	}
	

	
}



