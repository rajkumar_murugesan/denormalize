package com.pvamu.den.dal;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.pvamu.den.model.LogReport;


@Repository
public class LogReportDALImpl {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	

	
	
	public void addLog(LogReport logReport) {
		mongoTemplate.save(logReport);
		
	}
	
	public long collectionCount() {
		Query query = new Query();
		query.getLimit();
		return mongoTemplate.count(query, LogReport.class);
	}
	
	public void clearAllRecord() {
		mongoTemplate.dropCollection(LogReport.class);
		
	}
	
	public List<LogReport> getAllRecord() {
		
		return mongoTemplate.findAll(LogReport.class);
	}
	

}
